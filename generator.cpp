#include <File.h>
#include <String.h>
#include <Message.h>
#include <TypeConstants.h>
#include <type_traits>

static void makeStruct(const char *rustName, const char *path, size_t sz, size_t alignment) {
    BString contents("#[repr(C)]\npub struct ");
    contents.Append(rustName);
    contents.Append(" {\n    tag_: std::marker::PhantomData<*mut u8>,\n");
    contents << "    data_: [u" << alignment * 8 <<
      "; " << sz / alignment + (sz % alignment == 0 ? 0 : 1) <<
      "],\n";
    contents.Append("}\n");
    BFile file(path, B_CREATE_FILE | B_ERASE_FILE | B_WRITE_ONLY);
    file.WriteExactly(contents.String(), contents.Length());
}

static void btypedImpls(const char *path) {
  BFile file(path, B_CREATE_FILE | B_ERASE_FILE | B_WRITE_ONLY);
#define MAKE_INSTANCE(rtype, constant) \
  { \
    BString block("impl BTyped for "); \
    block << rtype << " {\n"; \
    block << "    const TYPE_CODE: support_defs::TypeCode = "; \
    block << constant << ";\n}\n"; \
    file.WriteExactly(block.String(), block.Length()); \
  }
  MAKE_INSTANCE("i8", B_INT8_TYPE)
  MAKE_INSTANCE("u8", B_UINT8_TYPE)
  MAKE_INSTANCE("i16", B_INT16_TYPE)
  MAKE_INSTANCE("u16", B_UINT16_TYPE)
  MAKE_INSTANCE("i32", B_INT32_TYPE)
  MAKE_INSTANCE("u32", B_UINT32_TYPE)
  MAKE_INSTANCE("i64", B_INT64_TYPE)
  MAKE_INSTANCE("u64", B_UINT64_TYPE)
  MAKE_INSTANCE("isize", B_SSIZE_T_TYPE)
  MAKE_INSTANCE("usize", B_SIZE_T_TYPE)
  MAKE_INSTANCE("app_kit::Message", B_MESSAGE_TYPE)
#undef MAKE_INSTANCE
}

int main(int argc, char **args) {
#define MAKE_STRUCT(cppName, rustName, filename) \
  { \
    BString path(args[1]); \
    path.Append("/"); \
    path.Append(filename); \
    makeStruct(rustName, path.String(), sizeof(cppName), std::alignment_of<cppName>::value); \
  }
  MAKE_STRUCT(BMessage, "Message", "message_struct.rs")
#undef MAKE_STRUCT
  {
  	BString path(args[1]);
  	path << "/" << "btyped_instances.rs";
  	btypedImpls(path.String());
  }
}

