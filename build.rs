static CPP_FILES: &'static [&'static str] = &["message"];

fn main() {
	let out_dir = std::env::var_os("OUT_DIR").unwrap();
	let out_dir = std::path::Path::new(&out_dir);
	let generator = out_dir.join("generator");
	std::process::Command::new("g++")
	    .arg("generator.cpp")
	    .arg("-lbe")
	    .arg("-o")
	    .arg(&generator)
	    .status().unwrap();
	std::process::Command::new(&generator).arg(&out_dir).status().unwrap();
    CPP_FILES
        .iter()
        .fold(cc::Build::new().cpp(true), |builder, name| {
        	println!("cargo:rerun-if-changed=wrappers/{}.cpp", name);
            builder.file(format!("wrappers/{}.cpp", name))
        })
        .cpp_link_stdlib("stdc++")
        .compile("hlimerick");
    println!("cargo:rustc-link-lib=dylib=be");
    println!("cargo:rerun-if-changed=generator.cpp");
    println!("cargo:rerun-if-changed=build.rs");
}
