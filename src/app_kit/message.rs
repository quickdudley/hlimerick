include!(concat!(env!("OUT_DIR"), "/message_struct.rs"));

impl Message {
    pub fn new() -> Self {
        let mut result = std::mem::MaybeUninit::uninit();
        unsafe {
            hlimerick_init_message_g(result.as_mut_ptr());
            result.assume_init()
        }
    }

    pub fn new_what(what: u32) -> Self {
        let mut result = std::mem::MaybeUninit::uninit();
        unsafe {
            hlimerick_init_message_w(result.as_mut_ptr(), what);
            result.assume_init()
        }
    }

    pub fn names<'a>(&'a self) -> Names<'a> {
        Names {
            parent: self,
            index: 0,
        }
    }
}

impl Clone for Message {
    fn clone(&self) -> Self {
        let mut result = std::mem::MaybeUninit::uninit();
        unsafe {
            hlimerick_copy_message(result.as_mut_ptr(), &*self);
            result.assume_init()
        }
    }
}

impl std::ops::Drop for Message {
    fn drop(&mut self) {
        unsafe {
            hlimerick_clean_message(self);
        }
    }
}

pub struct Names<'a> {
    parent: &'a Message,
    index: i32,
}

impl<'a> Names<'a> {
    fn get(&self) -> Result<<Self as Iterator>::Item, Box<dyn std::error::Error>> {
        let mut name: *const std::ffi::c_char = std::ptr::null();
        let mut type_code = 0;
        let mut count = 0;
        unsafe {
            let status = hlimerick_get_message_next_name(
                &*self.parent,
                1095653716, // B_ANY_TYPE
                self.index,
                &mut name,
                &mut type_code,
                &mut count,
            );
            if status == 0 {
                Ok(())
            } else {
                Err(std::io::Error::from_raw_os_error(status))
            }?;
            let name = std::ffi::CStr::from_ptr(name);
            Ok((name.to_str()?.to_owned(), type_code, count))
        }
    }
}

impl<'a> Iterator for Names<'a> {
    type Item = (String, crate::support_defs::TypeCode, i32);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.get() {
                Ok(result) => {
                    self.index += 1;
                    break Some(result);
                }
                Err(err) => match err.downcast::<std::str::Utf8Error>() {
                    Ok(_) => {
                        self.index += 1;
                    }
                    Err(_) => break None,
                },
            }
        }
    }
}

extern "C" {
    fn hlimerick_init_message_g(msg: *mut Message);
    fn hlimerick_init_message_w(msg: *mut Message, what: u32);
    fn hlimerick_copy_message(target: *mut Message, source: *const Message);
    fn hlimerick_clean_message(msg: *mut Message);
    fn hlimerick_get_message_next_name(
        msg: *const Message,
        requested: crate::support_defs::TypeCode,
        index: i32,
        found: *mut *const std::ffi::c_char,
        typeFound: *mut crate::support_defs::TypeCode,
        countFound: *mut i32,
    ) -> crate::support_defs::Status;
}
