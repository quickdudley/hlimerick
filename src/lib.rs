pub mod app_kit;
pub mod support_defs;

pub trait BTyped {
	const TYPE_CODE: support_defs::TypeCode;
}
include!(concat!(env!("OUT_DIR"), "/btyped_instances.rs"));

#[test]
fn test_u64() {
	assert_ne!(<u64 as BTyped>::TYPE_CODE, 0);
}