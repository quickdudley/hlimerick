#include <Message.h>

extern "C" {
void hlimerick_init_message_g(BMessage *blank) {
	new(blank) BMessage;
}

void hlimerick_init_message_w(BMessage *blank, uint32 what) {
	new(blank) BMessage(what);
}

void hlimerick_copy_message(BMessage *target, const BMessage *source) {
	*target = *source;
}

void hlimerick_clean_message(BMessage *msg) {
  msg->~BMessage();
}

status_t hlimerick_get_message_next_name(const BMessage *msg, type_code requested, int32 index, char **nameFound, type_code *typeFound, int32 *countFound) {
	return msg->GetInfo(requested, index, nameFound, typeFound, countFound);
}
}